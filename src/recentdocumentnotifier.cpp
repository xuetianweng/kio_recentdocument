#include <KDirWatch>
#include <KGlobal>
#include <KGlobalSettings>
#include <KPluginFactory>
#include <KPluginLoader>
#include <KStandardDirs>
#include <KUrl>
#include <KDirNotify>
#include <KRecentDocument>

#include "recentdocumentnotifier.h"

K_PLUGIN_FACTORY(RecentDocumentFactory, registerPlugin<RecentDocumentNotifier>();)
K_EXPORT_PLUGIN(RecentDocumentFactory("kio_recentdocument"))


RecentDocumentNotifier::RecentDocumentNotifier(QObject *parent, const QList<QVariant> &)
    : KDEDModule(parent)
{
    dirWatch = new KDirWatch(this);
    dirWatch->addDir(KRecentDocument::recentDocumentDirectory(), KDirWatch::WatchFiles);
    connect(dirWatch, SIGNAL(created(QString)), this, SLOT(dirty(QString)));
    connect(dirWatch, SIGNAL(deleted(QString)), this, SLOT(dirty(QString)));
    connect(dirWatch, SIGNAL(dirty(QString)), this, SLOT(dirty(QString)));

    connect(dirWatch, SIGNAL(dirty(QString)), SLOT(dirty(QString)));
}

void RecentDocumentNotifier::dirty(const QString &path)
{
    Q_UNUSED(path)

    if (path.endsWith(".desktop")) {
        // Emitting FilesAdded forces a re-read of the dir
        KUrl url("recentdocument:/");
        QFileInfo info(path);
        url.addPath(info.fileName());
        url.cleanPath();
        org::kde::KDirNotify::emitFilesAdded(url.url());
    }
}