#ifndef KCM_RECENT_DOCUMENT_H
#define KCM_RECENT_DOCUMENT_H

#include <KCModule>
#include <KPluginFactory>

namespace Ui
{
class KCMRecentDocument;
}

class KCMRecentDocument : public KCModule
{
    Q_OBJECT
public:
    KCMRecentDocument(QWidget* parent = 0, const QVariantList& args = QVariantList());
    virtual ~KCMRecentDocument();

    virtual void load();
    virtual void save();
    virtual void defaults();
private:
    Ui::KCMRecentDocument* ui;
};

#endif