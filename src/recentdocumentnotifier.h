#ifndef RECENTDOCUMENTNOTIFIER_H
#define RECENTDOCUMENTNOTIFIER_H

#include <KDEDModule>
#include <QtDBus/QtDBus>

class KDirWatch;

class RecentDocumentNotifier : public KDEDModule
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.kde.RecentDocumentNotifier")

public:
    RecentDocumentNotifier(QObject* parent, const QList<QVariant>&);

private slots:
    void dirty(const QString &path);

private:
    KDirWatch *dirWatch;
};

#endif