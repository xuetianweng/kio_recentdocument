#include <QCoreApplication>
#include <QDBusInterface>
#include <QFileInfo>

#include <KDebug>
#include <KComponentData>
#include <KRecentDocument>
#include <KDirWatch>
#include <KDesktopFile>
#include <KStandardDirs>
#include <KIO/Job>
#include <KIO/NetAccess>

#include <stdio.h>

#include "recentdocument.h"

extern "C" int KDE_EXPORT kdemain(int argc, char **argv)
{
    // necessary to use other kio slaves
    QCoreApplication app(argc, argv);
    KComponentData("kio_recentdocument", "kdelibs4");
    KGlobal::locale();
    if (argc != 4) {
        fprintf(stderr, "Usage: kio_recentdocument protocol domain-socket1 domain-socket2\n");
        exit(-1);
    }
    // start the slave
    RecentDocument slave(argv[2], argv[3]);
    slave.dispatchLoop();
    return 0;
}

bool isRootUrl(const KUrl& url)
{
    const QString path = url.path(KUrl::RemoveTrailingSlash);
    return(!url.hasQuery() &&
           (path.isEmpty() || path == QLatin1String("/")));
}

RecentDocument::RecentDocument(const QByteArray& pool, const QByteArray& app):
    ForwardingSlaveBase("recentdocument", pool, app)
{
    QDBusInterface kded("org.kde.kded", "/kded", "org.kde.kded");
    kded.call("loadModule", "recentdocumentnotifier");
}

RecentDocument::~RecentDocument()
{

}

bool RecentDocument::rewriteUrl(const KUrl& url, KUrl& newUrl)
{
    kDebug() << "rewriteUrl " << url;
    if (isRootUrl(url)) {
        newUrl = url;
        return !newUrl.isEmpty();
    } else {
        KUrl desktopFileUrl;
        desktopFileUrl.setProtocol("file");
        desktopFileUrl.setPath(KRecentDocument::recentDocumentDirectory());
        desktopFileUrl.addPath(url.path() + ".desktop");
        if (KDesktopFile::isDesktopFile(desktopFileUrl.toLocalFile())) {
            KDesktopFile file(desktopFileUrl.path());
            if (file.hasLinkType())
                newUrl = KUrl(file.readUrl());
        }
        return !newUrl.isEmpty();
    }
    return false;
}

void RecentDocument::listDir(const KUrl& url)
{
    if (isRootUrl(url)) {
        // flush
        listEntry(KIO::UDSEntry(), true);

        QStringList list = KRecentDocument::recentDocuments();
        KIO::UDSEntryList udslist;
        KIO::UDSEntry uds;
        QSet<QString> urlSet;
        Q_FOREACH(const QString & entry, list) {
            KUrl url;
            if (KDesktopFile::isDesktopFile(entry)) {
                QFileInfo info(entry);
                KDesktopFile file(entry);

                KUrl urlInside(file.readUrl());
                if (urlInside.protocol() == "recentdocument")
                    continue;

                url = KUrl(entry);

                KUrl urlToState;

                if (urlInside.isLocalFile())
                    urlToState = urlInside;
                else
                    urlToState = url;

                if (!urlToState.isEmpty()) {
                    if (KIO::StatJob* job = KIO::stat(urlToState, KIO::HideProgressInfo)) {
                        // we do not want to wait for the event loop to delete the job
                        QScopedPointer<KIO::StatJob> sp(job);
                        job->setAutoDelete(false);
                        if (KIO::NetAccess::synchronousRun(job, 0)) {
                            uds = job->statResult();
                        }
                    }
                }

                QString prettyUrl = urlInside.prettyUrl();
                if (uds.count() && !urlSet.contains(prettyUrl)) {
                    urlSet.insert(prettyUrl);
                    uds.insert(KIO::UDSEntry::UDS_NAME, info.completeBaseName());

                    if (urlInside.isLocalFile()) {
                        uds.insert(KIO::UDSEntry::UDS_DISPLAY_NAME, urlInside.toLocalFile());
                        uds.insert(KIO::UDSEntry::UDS_LOCAL_PATH, urlToState.path());
                    } else {
                        uds.insert(KIO::UDSEntry::UDS_DISPLAY_NAME, prettyUrl);
                    }
                    uds.insert(KIO::UDSEntry::UDS_TARGET_URL, prettyUrl);
                    udslist << uds;
                }
            }
        }
        listEntries(udslist);

        listEntry(KIO::UDSEntry(), true);
        finished();
    }
}

void RecentDocument::prepareUDSEntry(KIO::UDSEntry& entry, bool listing) const
{
    kDebug() << "prepareUDSEntry";
    ForwardingSlaveBase::prepareUDSEntry(entry, listing);
}

QString RecentDocument::desktopFile(KIO::UDSEntry& entry) const
{
    const QString name = entry.stringValue(KIO::UDSEntry::UDS_NAME);
    if (name == "." || name == "..")
        return QString();

    KUrl url = processedUrl();
    url.addPath(name);

    if (KDesktopFile::isDesktopFile(url.path()))
        return url.path();

    return QString();
}

void RecentDocument::stat(const KUrl& url)
{
    if (isRootUrl(url)) {
        kDebug() << "Stat root" << url;
        //
        // stat the root path
        //
        KIO::UDSEntry uds;
        uds.insert(KIO::UDSEntry::UDS_NAME, i18n("Recent Document"));
        uds.insert(KIO::UDSEntry::UDS_DISPLAY_NAME, i18n("Recent Document"));
        uds.insert(KIO::UDSEntry::UDS_DISPLAY_TYPE, i18n("Recent Document"));
        uds.insert(KIO::UDSEntry::UDS_ICON_NAME, QString::fromLatin1("document-open-recent"));
        uds.insert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
        uds.insert(KIO::UDSEntry::UDS_MIME_TYPE, QString::fromLatin1("inode/directory"));

        statEntry(uds);
        finished();
    }
    // results are forwarded
    else {
        kDebug() << "Stat forward" << url;
        ForwardingSlaveBase::stat(url);
    }
}

void RecentDocument::mimetype(const KUrl& url)
{
    kDebug() << url;

    // the root url is always a folder
    if (isRootUrl(url)) {
        mimeType(QString::fromLatin1("inode/directory"));
        finished();
    }
    // results are forwarded
    else {
        ForwardingSlaveBase::mimetype(url);
    }
}

void RecentDocument::del(const KUrl& url, bool isFile)
{
    ForwardingSlaveBase::del(url, isFile);
}