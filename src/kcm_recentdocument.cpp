#include <KGenericFactory>
#include <KGlobalSettings>

#include "kcm_recentdocument.h"
#include "ui_kcm_recentdocument.h"

K_PLUGIN_FACTORY_DECLARATION(KcmRecentDocumentFactory);

const int maxEntryDefault = 10;
const bool useRecentDefault = true;

KCMRecentDocument::KCMRecentDocument(QWidget* parent, const QVariantList& args):
    KCModule(KcmRecentDocumentFactory::componentData(), parent, args),
    ui(new Ui::KCMRecentDocument)
{
    ui->setupUi(this);
    connect(ui->checkBoxUseRecent, SIGNAL(clicked(bool)), this, SLOT(changed()));
    connect(ui->spinEntryNumber, SIGNAL(valueChanged(int)), this, SLOT(changed()));
}

KCMRecentDocument::~KCMRecentDocument()
{
    delete ui;
}

void KCMRecentDocument::load()
{
    KConfigGroup config = KSharedConfig::openConfig("kdeglobals")->group(QByteArray("RecentDocuments"));
    bool useRecent = config.readEntry(QLatin1String("UseRecent"), useRecentDefault);
    int maxEntry = config.readEntry(QLatin1String("MaxEntries"), maxEntryDefault);
    qDebug() << useRecent;
    ui->checkBoxUseRecent->setChecked(useRecent);
    ui->spinEntryNumber->setValue(maxEntry);
}

void KCMRecentDocument::defaults()
{
    ui->checkBoxUseRecent->setChecked(useRecentDefault);
    ui->spinEntryNumber->setValue(maxEntryDefault);
    changed();
}

void KCMRecentDocument::save()
{
    KConfigGroup config = KSharedConfig::openConfig("kdeglobals")->group(QByteArray("RecentDocuments"));
    config.writeEntry(QLatin1String("UseRecent"), ui->checkBoxUseRecent->isChecked());
    qDebug() << ui->checkBoxUseRecent->isChecked();
    config.writeEntry(QLatin1String("MaxEntries"), ui->spinEntryNumber->value());
}
